## 如何添加参考文献

GitBook项目创建时间不长，有些功能可能还没有插件，或者有些插件的功能并不完善，比如参考文献的插件。GitBook有多个使用bibtex生成参考文献的格式，多数很少更新，且功能过于简单。

### 安装和配置
* 在`book.json`中添加如下内容后运行`gitbook install`:

```json
 "plugins": ["bibtex-indexed-cite"],


 "bibtex-indexed-cite": {
            "path": "/"
        }
```
其中path用来指定参考文献库“literature.bib”所在的路径。

* 在项目根目录，新增literature.bib和References.md两个文件，其中literature.bib用来存放参考文献数据，References.md文件中写入如下内容：

```md
{% references %} {% endreferences %}
```

### 用法

在需要引用参考文献的地方使用如下命令：`{{"GitBook.com-2017"|cite}}`

```md
{{ "TLW" | cite }}
```

需要注意，第一、引用名中不能有中文,如`{{"GitBook.com-2017"|cite}}`不能为`{{"中文名称-2017"|cite}}`；第二、参考文献需要单独用一个文件生成，文件名为“References.md”。

### 设定参考文献样式

`bibtex-indexed-cite`插件，目前只支持IEEE的引文格式，且引用没有上标，可通过自定义样式表实现上标效果：

```css
a[href*="#cite"] {
    vertical-align: super;
    font-size: 0.8em;
}
```

