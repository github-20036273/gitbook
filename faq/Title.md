## 如何更改电子书标题
在项目根目录新建book.json的配置文件，写入诸如如下信息：

``` json
{
    "title": "GitBook学习笔记",
    "author": "yangjh"
}
```
