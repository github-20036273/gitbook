## 为左侧导航栏添加链接信息

在book.json文件中，添加links配置项：
```json
"links" : {
    "sidebar" : {
        "Home" : "http://yangzh.cn"
    }
}
```
