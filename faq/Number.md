## 如何为目录添加序号

默认情况下，GitBook的目录是没有序号的，若想为目录编号，需要在book.json中添加如下配置：

```json
"theme-default": {
            "showLevel": true
        },
```
