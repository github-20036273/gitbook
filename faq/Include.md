## 如何引入外部文件

在有些情景下，我们需要将外部独立的文件加入到文档中。[include codeblock插件][1]可以满足这个需求。

### 安装并配置插件
include Codeblock插件可能需要和ace插件配合，因此，在book.json文件中，加入如下内容：

```json
"plugins": [
        "include-codeblock",
        "ace"
    ],
"include-codeblock": {
    "template": "ace",
    "unindent": true,
    "edit": true
    }
```

### 在文档中引入外部文件
[import](include.md)

更加详细的用法，可以查看[include codeblock插件手册][1]。

[1]: https://plugins.gitbook.com/plugin/include-codeblock
