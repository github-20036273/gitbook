## 常用的插件都有哪些？

与Latex制作的pdf相比，GitBook的最大优势在于其能使用js脚本，这使得GitBook在功能上可以不断拓展，比如在文档中嵌入视频、嵌入动画、嵌入js练习，嵌入测试。以下是一些常用的插件：

* [exercises](https://plugins.gitbook.com/plugin/exercises)，在文档中增加交互练习内容，目前只支持js语言。
* [quiz](https://github.com/chudaol/gitbook-plugin-quiz)，在文档中增加测验内容，支持单选、多选、排序。
* [include-codeblock](https://plugins.gitbook.com/plugin/include-codeblock)，使得GitBook能引用外部独立文档。
* [localized-footer](https://github.com/noerw/gitbook-plugin-localized-footer#readme)，为GitBook的每个页面添加页脚内容。
* [search-pro](https://plugins.gitbook.com/plugin/search-pro)，为GitBook添加多字节字符搜索，实现中文搜索（默认只能搜索英文）。
* [sharing-plus](https://plugins.gitbook.com/plugin/sharing-plus)，GitBook默认分享工具的增强版，加入了中国常用的社交网站。
* [changyan](https://github.com/codepiano/gitbook-plugin-changyan)，为GitBook页面添加畅言评论框。
* [iframely](https://plugins.gitbook.com/plugin/iframely), 在页面中嵌入常见视频网站内容。
* [bibtex-indexed-cite](https://plugins.gitbook.com/plugin/bibtex-indexed-cite)，使用bibtex格式，自动生成参考文献。

### 本文档用到的插件及配置
[import](../book.json)


