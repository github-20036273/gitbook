## 在页面中添加测验功能

### 添加quiz插件
在book.json中添加如下内容:

```json
{
    "plugins": ["quiz"]
}
```

运行gitbook install。

### 设置插件配置信息

```json
        "quiz": {
            "labels": {
                "check": "提交答案",
                "showExplanation": "显示解释",
                "showCorrect": "显示正确答案",
                "explanationHeader": "释义"
            },
            "text": {
                "noChosen": "Choose at least one answer",
                "incomplete": "Some correct answers are missing"
            },
            "buttons": {
                "showCorrect": true,
                "showExplanation": true
            }
        }
```


### 在md文件中插入quiz

<quiz name="Gitbook Quiz">
    <question multiple>
        <p>What is gitbook used for?</p>
        <answer correct>To read books</answer>
        <answer>To book hotel named git</answer>
        <answer correct>To write and publish beautiful books</answer>
        <explanation>GitBook.com lets you write, publish and manage your books online as a service.</explanation>
    </question>
    <question>
        <p>Is it quiz?</p>
        <answer correct>Yes</answer>
        <answer>No</answer>
    </question>
    <question>
        <p>This is multiple dropdown quiz, in each dropdown select a correct number corresponding to the dropdown's order</p>
        <answer>
            <option correct>First</option>
            <option>Second</option>
            <option>Third</option>
            <option>Fourth</option>
        </answer>
        <answer>
            <option>First</option>
            <option correct>Second</option>
            <option>Third</option>
            <option>Fourth</option>
        </answer>
        <answer>
            <option>First</option>
            <option>Second</option>
            <option correct>Third</option>
            <option>Fourth</option>
        </answer>
        <answer>
            <option>First</option>
            <option>Second</option>
            <option>Third</option>
            <option correct>Fourth</option>
        </answer>
    </question>
</quiz>

