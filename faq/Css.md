## 如何自定义样式

由于MarkDown语法本身是HTML语言的一个子集，按照Web标准的精神（内容、表现和行为三者分离），MarkDown语法中，没有和内容表现形式相关的语法规则。因此，要实现自定义样式，必须从CSS或者插件入手。

据[GitBook官方手册][1]介绍，我们可以通过在项目文件夹中创建特定样式表的方式达到自定义样式的目的：

> You can specify CSS files to be included in your book's website or PDF builds by creating files:

>    styles/website.css: will apply only to the website

>    styles/pdf.css: will apply only to the PDF

>    styles/ebook.css: will apply only to ebook formats (PDF, Mobi, ePub)

[1]: https://help.gitbook.com/content/how-can-i-include-css.html

