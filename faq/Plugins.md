## 如何安装插件

插件是GitBook拓展功能的主要机制，插件的使用步骤如下：

### 1. 在book.json文件中添加插件名称

book.json文件中，plugins选项用来添加、关闭插件，如下面的内容表示关闭sharing插件，使用sharing-plus插件：
``` json
"plugins": ["-sharing", "sharing-plus"],
```
所有插件名称可从[https://plugins.gitbook.com/][1]获取。

### 2. 安装插件
执行下面的命令，gitbook会自动安装所需插件：

``` sh
gitbook install
```
### 3. 设置插件配置

一般而言，插件都有默认配置，如果需要的话，在book.json中加入插件配置内容即可。例如：

```json
    "pluginsConfig": {
        "sharing": {
            "douban": false,
            "facebook": false,
            "google": false,
            "hatenaBookmark": false,
            "instapaper": false,
            "line": false,
            "linkedin": false,
            "messenger": false,
            "pocket": false,
            "qq": true,
            "qzone": true,
            "stumbleupon": false,
            "twitter": false,
            "viber": false,
            "vk": false,
            "weibo": true,
            "whatsapp": false,
            "all": [
                "facebook", "google", "twitter",
                "weibo", "qq", "linkedin",
                "qzone", "douban"
            ]
        }
    }
```
[1]: https://plugins.gitbook.com/
