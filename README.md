# GitBook 学习笔记
这本电子书主要记录GitBook工具使用过程中遇到的疑问及解决办法。虽然网络中已经有很多GitBook的资料，但学习技能的最好办法还是实践，而不仅仅是阅读。

这本小书假定读者已经具有操作系统的基本知识，尤其是命令行（终端）和文本编辑器的相关知识。

## 什么是GitBook?
GitBook 有两个含义：一个是基于[Node.js][1]的制作电子书的命令行工具，其作品如[ReduxJS documentation][2]，一个是提供书写、托管服务的在线平台[GitBook][gitbook]。本书是对GitBook命令行工具学习过程的记录。

## 主要参考资源

* [GitBook主页](https://www.gitbook.com/)
* [Github地址](https://github.com/GitbookIO/)
* [GitBook编辑器](https://www.gitbook.com/editor/osx)
* [GitBook Toolchain Documentation](http://toolchain.gitbook.com/)
* [GitBook Documentation](http://help.gitbook.com/)
* [GitBook使用教程](http://gitbook.zhangjikai.com/)

[1]: https://nodejs.org/en/ "nodejs官方网站"
[2]: http://redux.js.org/ "Redux手册"
[gitbook]: https://www.gitbook.com

