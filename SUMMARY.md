# Summary

### 基础问题
* [简介](README.md)
* [使用GitBook制作文档的步骤是怎样的](faq/Step.md)
* [如何制作多级目录](faq/Contents.md)

### markdown
* [如何插入图片](faq/Image.md)
* [如何在表格的内容中使用竖线|](md/pipe.md)
* [如何在url中使用括号](md/url.md)

### 功能拓展
* [如何安装插件](faq/Plugins.md)
* [常用的插件都有哪些](faq/Recommand.md)
* [如何高亮代码](faq/Highlight.md)
* [如何引入外部文件](faq/Include.md)
* [添加测验功能](faq/Exercise.md)
* [如何添加参考文献](faq/Reference.md)

### 如何定制
* [如何更改电子书的标题](faq/Title.md)
* [如何自定义样式](faq/Css.md)
* [如何为目录添加序号](faq/Number.md)
* [如何在左侧导航栏添加链接](faq/Sidebar.md)

### 托管问题
* [如何托管到Pages上](UsingPages.md)

### 多人协作
* [如何实现多人协作](Collaborate.md)

----

* [参考文献](References.md)

